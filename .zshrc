autoload -U compinit; compinit
autoload -U colors; colors
autoload -U promptinit; promptinit

unsetopt beep
setopt PROMPT_SUBST
setopt NO_CORRECT

zstyle ':completion:*:cd:*' tag-order local-directories
zstyle ':completion:*' menu select
unsetopt menu_complete

ZSH=$HOME/.zsh
source $ZSH/git.zsh
source $ZSH/functions.zsh

bindkey -e
export EDITOR="vim"
bindkey '^[[Z' reverse-menu-complete

alias ls="ls -hGp"

HISTFILE=$HOME/.zsh_history
HISTSIZE=10000
SAVEHIST=10000
setopt APPEND_HISTORY
setopt EXTENDED_HISTORY
setopt HIST_EXPIRE_DUPS_FIRST
setopt HIST_FIND_NO_DUPS
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_IGNORE_DUPS
setopt HIST_REDUCE_BLANKS
setopt HIST_SAVE_NO_DUPS
setopt INC_APPEND_HISTORY
setopt SHARE_HISTORY

# export WORKON_HOME=$HOME/.virtualenvs
# export PROJECT_HOME=$HOME/projects
# export VIRTUAL_ENV_DISABLE_PROMPT=true
# source "/usr/local/bin/virtualenvwrapper.sh"
